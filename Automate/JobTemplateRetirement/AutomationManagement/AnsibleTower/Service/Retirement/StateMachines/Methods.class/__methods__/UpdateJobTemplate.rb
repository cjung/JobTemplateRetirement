#
#            Automate Method
#
# Call Tower retirement job template
# inspired by: https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/tower_related_automate_components/chapter.html
#
# This Method will retrieve the job template name from the service object
# and override it with a "-retirement" job template name
#
# Copyright (C) 2017, Christian Jung
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

begin
    $evm.log("info", "EVM Automate Method Started")
  
    # Dump all of root's attributes to the log
    $evm.root.attributes.sort.each { |k, v| $evm.log("info", "Root:<$evm.root> Attribute - #{k}: #{v}")}

    job_template = $evm.root['service'].job_template
    $evm.log("info", "Job template: #{job_template.inspect}")

    ANSIBLE_NAMESPACE = 'AutomationManagement/AnsibleTower/Operations'.freeze
    ANSIBLE_STATE_MACHINE_CLASS = 'JobTemplate'.freeze
    #ANSIBLE_STATE_MACHINE_INSTANCE = 'CatalogItemInitialization'.freeze
    #VM_CLASS = 'VmOrTemplate'.freeze
    attrs = {}
    ANSIBLE_STATE_MACHINE_INSTANCE = "GhostLab-Deprovision"
    attrs = $evm.root['service'].dialog_options
    $evm.log("info", "Attributes: #{attrs.inspect}")

    # get IP from custom attribute
    service = $evm.root['service']
    attrs["deprovision-phpipam_ip"]=service.custom_get("phpipam_ip") unless service.custom_get("phpipam_ip").nil?

    options = {}
    options[:namespace]     = ANSIBLE_NAMESPACE
    options[:class_name]    = ANSIBLE_STATE_MACHINE_CLASS
    options[:instance_name] = ANSIBLE_STATE_MACHINE_INSTANCE
    options[:user_id]       = $evm.root['user'].id
    options[:attrs]         = attrs
    auto_approve            = true
    $evm.execute('create_automation_request', options, $evm.root['user'].userid, auto_approve)
    $evm.log("info", "running create_automation_request, with #{options}, $evm.root['user'].userid, auto_approve")
    
    #
    # Exit method
    #
    $evm.log("info", "EVM Automate Method Ended")
    exit MIQ_OK
  
    #
    # Set Ruby rescue behavior
    #
  rescue => err
    $evm.log("error", "[#{err}]\n#{err.backtrace.join("\n")}")
    exit MIQ_ABORT
  end
