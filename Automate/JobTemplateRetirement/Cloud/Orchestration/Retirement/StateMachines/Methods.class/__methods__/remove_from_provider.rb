#
# Description: This method removes the stack from the provider
#

# Get stack from root object
stack = $evm.root['orchestration_stack']

# do not run delete methods below, if it's a tower job or they will fail
if stack.type == "ManageIQ::Providers::AnsibleTower::AutomationManager::Job"
  $evm.log("info", "This is an Ansible Tower Job, skipping")
  exit MIQ_OK
end

if stack
  ems = stack.ext_management_system
  if stack.raw_exists?
    $evm.log('info', "Removing stack:<#{stack.name}> from provider:<#{ems.try(:name)}>")
    stack.raw_delete_stack
    $evm.set_state_var('stack_exists_in_provider', true)
  else
    $evm.log('info', "Stack <#{stack.name}> no longer exists in provider:<#{ems.try(:name)}>")
    $evm.set_state_var('stack_exists_in_provider', false)
  end
end
