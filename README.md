# README

This code add retirement state machines for Ansible Tower Jobs to CloudForms. Written and tested with Version 4.6.

The goal was to find a way which is least intrusive and does as little as possible changes in Automate and also reuse existing code as much as possible.

## Status

This code was written during a POC and should not be used in Production.

## Assumptions

To make this work, a few things have been assumed:

- for every provisioning job template in Tower, there is a retirement job template with the name suffix "-retired". ***Note***: In the current code, the name is hard coded ("Ghostlab-Deprovision").

## Changes quickly explained

A new State Machine was created under AutomationManagement, AnsibleTower, Service, Retirement. It's originally a copy of the Service Retirement State Machine.

UpdateJobTemplate Method: this method retrieves the dialog_options from the service object and a custom attribute (if defined) to send all this data to run an Ansible Tower Retirement Job.

The method will also check for a custom attribute stored on the parent Service object, and if it exists, it will be send an extra var to the Tower Job Template.

remove_from_provider: this method had to be adjusted since CloudForms rung the Cloud, Orchestration, Retirement logic during retirement. This logic tries to run a method "raw_delete" which fails. The change will skip this method if an Ansible Tower Job was detected.
